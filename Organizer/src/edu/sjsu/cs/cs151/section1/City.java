package edu.sjsu.cs.cs151.section1;

public class City {
	private StateProvince parentStateProvince;
	private String name;
	
	public City(StateProvince parentStateProvince, String name) {
		this.parentStateProvince = parentStateProvince;
		this.name = name;
		this.parentStateProvince.cities.add(this);
	}
	
	public Country getParentCountry() {
		return this.parentStateProvince.parentCountry;
	}
	
	public StateProvince getParentStateProvince() {
		return parentStateProvince;
	}
	
	public void setParentStateProvince(StateProvince parentStateProvince) {
		this.parentStateProvince = parentStateProvince;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
