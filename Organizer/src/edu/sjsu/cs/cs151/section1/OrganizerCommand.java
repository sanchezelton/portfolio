package edu.sjsu.cs.cs151.section1;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

/**
 * A typesafe enum pattern class for Organizer commands
 * 
 * @author Elton R. Sanchez, (C) 2018 All Rights Reserved
 * @version 06/23/2018
 */
public class OrganizerCommand {
	private final String name;
	
	private OrganizerCommand(String name) {
		this.name = name;
	}
	
	public String toString() {
		return this.name;
	}
	
	// flags for command execution; made public for code using the organizer
	public static final OrganizerCommand COMMAND_ADD_FACT = new OrganizerCommand("COMMAND_ADD_FACT"); //NOI18N
	public static final OrganizerCommand COMMAND_CHANGE_FACTTYPE = new OrganizerCommand("COMMAND_CHANGE_FACTTYPE"); //NOI18N
	public static final OrganizerCommand COMMAND_SHOW_ALL_FACTS = new OrganizerCommand("COMMAND_SHOW_ALL_FACTS"); //NOI18N
	public static final OrganizerCommand COMMAND_SHOW_TODO_LIST = new OrganizerCommand("COMMAND_SHOW_TODO_LIST"); //NOI18N
	public static final OrganizerCommand COMMAND_SHOW_QUOTES = new OrganizerCommand("COMMAND_SHOW_QUOTES"); //NOI18N
	public static final OrganizerCommand COMMAND_SHOW_CONTACTS = new OrganizerCommand("COMMAND_OPEN_FILE"); //NOI18N
	public static final OrganizerCommand COMMAND_SAVE_FILE = new OrganizerCommand("COMMAND_SAVE_FILE"); //NOI18N
	public static final OrganizerCommand COMMAND_HTML_BUILD = new OrganizerCommand("COMMAND_HTML_BUILD"); //NOI18N
	public static final OrganizerCommand COMMAND_CLEAR_FACTS = new OrganizerCommand("COMMAND_CLEAR_FACTS"); //NOI18N
	public static final OrganizerCommand COMMAND_EXIT = new OrganizerCommand("COMMAND_EXIT"); //NOI18N
	
	public static final List<OrganizerCommand> ALL_COMMANDS = new ArrayList<OrganizerCommand>(Arrays.asList(
			COMMAND_ADD_FACT,
			COMMAND_CHANGE_FACTTYPE,
            COMMAND_SHOW_ALL_FACTS,
            COMMAND_SHOW_TODO_LIST,
            COMMAND_SHOW_QUOTES,
            COMMAND_SHOW_CONTACTS,
            COMMAND_SAVE_FILE,
            COMMAND_HTML_BUILD,
            COMMAND_CLEAR_FACTS,
            COMMAND_EXIT
      ));
}