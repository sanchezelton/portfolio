package edu.sjsu.cs.cs151.section1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PhoneNumber {
	private List<Character>  countryCode = new ArrayList<Character>();	// aka TC, trunk code, more specifically, CC country calling code
	private List<Character> areaCode = new ArrayList<Character>();		// aka NDC, national destination code
	private List<Character>  number = new ArrayList<Character>();		// aka SN, subscriber number
	
	public List<Character> getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(List<Character> countryCode) {
		for (Iterator<Character> i = countryCode.iterator(); i.hasNext(); ) {
			if (!Character.isDigit(i.next().charValue())) {
				throw new NumberFormatException();
			}
		}
		this.countryCode = countryCode;
	}
	
	public List<Character> getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(List<Character> areaCode) {
		for (Iterator<Character> i = areaCode.iterator(); i.hasNext(); ) {
			if (!Character.isDigit(i.next().charValue())) {
				throw new NumberFormatException();
			}
		}
		this.areaCode = areaCode;
	}
	
	public List<Character> getNumber() {
		return number;
	}
	public void setNumber(List<Character> number) {
		for (Iterator<Character> i = number.iterator(); i.hasNext(); ) {
			if (!Character.isDigit(i.next().charValue())) {
				throw new NumberFormatException();
			}
		}
		this.number = number;
	}

}
