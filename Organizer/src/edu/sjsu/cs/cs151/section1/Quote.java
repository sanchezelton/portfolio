package edu.sjsu.cs.cs151.section1;

import java.util.ResourceBundle;

public class Quote extends Fact {
	private String author;
	
	public Quote(String quote, String author) {
		super(quote);
		this.author = author;
	}
	
	public Quote(String quote) {
		super(quote);
		this.author = ResourceBundle.getBundle("labels").getString("STRING_Anonymous"); //NOI18N
	}
	
	public String getFactText() {
		ResourceBundle bundle = ResourceBundle.getBundle("labels");							//NOI18N
		String quotationMarks = bundle.getString("STRING_QuotationMarks"); 			//NOI18N
		String quote = quotationMarks + this.factText + quotationMarks;
		quote += bundle.getString("STRING_AttributionMark"); 							//NOI18N
		quote += this.author;
		return quote;
	}
}
