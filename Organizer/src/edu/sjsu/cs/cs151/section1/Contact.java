package edu.sjsu.cs.cs151.section1;

import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Contact extends Fact {
	
	private String streetAddress1;
	private String streetAddress2;
	private City city;
	private StateProvince stateProvince;
	private String postalCode;
	private Country country;
	
	private PhoneNumber phone;
	private String email;
	private URL url;
	
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	
	public Contact(String name) {
		super(name);
	}

	public String getStreetAddress() {
		return streetAddress1;
	}

	public void setStreetAddress(String streetAddress1) {
		this.streetAddress1 = streetAddress1;
	}

	public String getStreetAddress2() {
		return streetAddress2;
	}

	public void setStreetAddress2(String streetAddress2) {
		this.streetAddress2 = streetAddress2;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public StateProvince getStateProvince() {
		return stateProvince;
	}

	public void setStateProvince(StateProvince stateProvince) {
		this.stateProvince = stateProvince;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
	
	public void setEmail(String email) throws InvalidEmailException {
		Matcher m = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
		if (m.find()) {
			this.email = email;
		} else {
			throw new InvalidEmailException();
		}
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public boolean isValidAddress(int isoLevel) throws IndexOutOfBoundsException {
		if (isoLevel == 0) {
			return true;
		} 
		if (isoLevel == 1) {
			return Country.ALL_COUNTRIES.contains(this.country);
		} else if (isoLevel == 2) {
			return Country.ALL_COUNTRIES.contains(this.country) &&
				   this.country.stateProvinceList.contains(this.stateProvince);
		} else if (isoLevel == 3) {
			return Country.ALL_COUNTRIES.contains(this.country) &&
					   this.country.stateProvinceList.contains(this.stateProvince) &&
					   this.stateProvince.cities.contains(this.city);
		} else {
			throw new IndexOutOfBoundsException();
		}
	}

	public PhoneNumber getPhoneNumber() {
		return phone;
	}

	public void setPhoneNumber(PhoneNumber phone) {
		this.phone = phone;
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}
}
