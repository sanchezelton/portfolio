package edu.sjsu.cs.cs151.section1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FactType {
	private final String name;
	
	private FactType(String name) {
		this.name = name;
	}
	
	public String toString() {
		return this.name;
	}
	
	/**
	 * Designation for generic fact adding mode
	 */
	public static final FactType FACTTYPE_GENERIC = new FactType("FACTTYPE_GENERIC"); //NOI18N

	/**
	 * Designation for quote fact adding mode
	 */
	public static final FactType FACTTYPE_QUOTE= new FactType("FACTTYPE_QUOTE"); //NOI18N
	
	/**
	 * Designation for to do item fact adding mode
	 */
	public static final FactType FACTTYPE_TODOITEM = new FactType("FACTTYPE_TODOITEM");	//NOI18N
	
	/**
	 * Designation for contact fact adding mode
	 */
	public static final FactType FACTTYPE_CONTACT = new FactType("FACTTYPE_CONTACT"); //NOI18N
	
	public static final List<FactType> ALL_FACTTYPES = new ArrayList<FactType>(Arrays.asList(
			FACTTYPE_GENERIC,
			FACTTYPE_QUOTE,
			FACTTYPE_TODOITEM,
			FACTTYPE_CONTACT
			));
}
