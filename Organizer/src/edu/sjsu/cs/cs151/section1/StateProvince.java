package edu.sjsu.cs.cs151.section1;

import java.util.HashSet;
import java.util.Set;

public class StateProvince {
	protected Set<City> cities = null;
	private String name;
	protected Country parentCountry;
	
	public StateProvince(Country country, String name) {
		this.parentCountry = country;
		this.setName(name);
		this.parentCountry.stateProvinceList.add(this);
	}
	
	public Set<City> getCities() {
		if (this.cities == null) {
			
		}
		return this.cities;
	}
	
	public static Set<StateProvince> getStateProvinces(Country country) {
		Set<StateProvince> results = new HashSet<StateProvince>();
		country.getCountryCode();
		return results;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
