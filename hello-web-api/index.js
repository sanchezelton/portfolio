
const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const HTTP_BAD_REQUEST = 400;

// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());
app.use(bodyParser.text());

let visitCounts = {};
let orderOps = {
    '*': function(a, b) {
        return a * b;
    },
    '/': function(a, b) {
        return a / b;
    },
    '+': function(a, b) {
        return a + b;
    },
    '-': function(a, b) {
        return a - b;
    }
};

function processExpr(expression) {
    console.log('processExpr: initial expression... ' + expression);
    let operators = Object.keys(orderOps);
    let opLen = operators.length;
    let matchOpIdx = false;
    for (let i = 0; i < opLen; i++) {
        while (matchOpIdx >= 0) {
            matchOpIdx = expression.indexOf(operators[i]);
            if (matchOpIdx < 0) {
                continue;
            }
            let matchedExpr = expression.substr(operators[i]);
            let nextOpIdx = matchOpIdx + matchedExpr.search(new RegExp('\\' + Object.keys(orderOps).join('|\\')));
            let sRegExp = '(\\k+)\\s*\\' + operators[i] + '\\s*(\\k+)';
            let matches = expression.substr(matchOpIdx, nextOpIdx).matches(new RegExp(sRegExp, 'i'));
            if (matches.length < 2) {
                throw new Error('processExpr: unexpected results for expression ' + expression + ' with regexp')
            }
            let a = matches[1];
            let b = matches[2];
            expression = expression.replace(matchedExpr, orderOps[operators[i]](a, b));
            console.log('processExpr: simplified... ' + matchedExpr + ' to ' + expression);
        }
    }
    console.log('processExpr: result... ' + expression);
    return expression;
}

app.get('/', function (req, res) {
    let ipAddr = req.ip;
    if (visitCounts[ipAddr]) {
        visitCounts[ipAddr]++;
    } else {
        visitCounts[ipAddr] = 1;
    }
    res.send('' + visitCounts[ipAddr]);
});

app.post('/math', function (req, res) {
    console.log('POST to /math received, body:');
    console.log(JSON.stringify(req.body, null, 2));
    let args = req.body.trim().split(' ');
    let argsLen = args.length;
    let tree = {};
    let expr = args.join('');
    try {
        res.send('' + processExpr(expr));
    } catch (e) {
        console.error(e);
        res.sendStatus(HTTP_BAD_REQUEST);
    }
});

app.get('/game', function (req, res) {
    res.send(visitCounts[ipAddr]);
});

app.listen(3000, function () {
  console.log('Challenge web app API listening on port 3000!')
});
