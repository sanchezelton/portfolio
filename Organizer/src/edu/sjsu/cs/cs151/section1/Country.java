package edu.sjsu.cs.cs151.section1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.neovisionaries.i18n.CountryCode;

public class Country {
	public static final Set<Country> ALL_COUNTRIES;
	public static final Country NONE;
	public CountryCode code;
	protected List<StateProvince> stateProvinceList;
	
	static {
		HashSet<Country> countryList = new HashSet<Country>();
//		String [] countryCodes = Locale.getISOCountries();
		CountryCode [] countryCodes = CountryCode.values();
//		for (String code : countryCodes) {
		for (CountryCode code : countryCodes) {
//			Locale locale = new Locale("", code.getCountry());
//			countryList.add(new Country(code, locale.getDisplayCountry()));
			countryList.add(new Country(code, code.toLocale().getDisplayCountry()));
		}
		ALL_COUNTRIES = countryList;
		NONE = new Country(null, "");
	}
	
	private String name;
	
	private Country(CountryCode code, String name) {
		this.code = code;
		this.name = name;
		this.stateProvinceList = new ArrayList<StateProvince>();
	}
	
	public String getCountryName() {
		return this.name;
	}
	
	public CountryCode getCountryCode() {
		return this.code;
	}
	
	public static Country getCountry(String code) {
		Country country = null;
		for (Iterator<Country> i = ALL_COUNTRIES.iterator(); i.hasNext(); country = i.next() ) {
			if (country != null && country.getCountryCode().toLocale().getCountry().equals(code)) {
				return country;
			}
		}
		return NONE;
	}
}
