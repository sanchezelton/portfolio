package edu.sjsu.cs.cs151.section1;

public class Fact {
	protected String factText;
	
	public Fact() {
		this.factText = "";
	}
	
	public Fact(String factText) {
		this.factText = factText;
	}
	
	public String getFactText() {
		return this.factText;
	}
	
	public String toString() {
		return this.getFactText();
	}
}
