package edu.sjsu.cs.cs151.section1;

import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ToDoItem extends Fact {
	private boolean done = false;
	private boolean isOverdue = false;
	private Calendar dueDate;
	private Calendar doneDate;
	
	public ToDoItem(String task, Calendar dueDate) {
		super(task);
		this.dueDate = dueDate;
	}
	
	public void markAsDone() {
		this.done = true;
		Calendar now = new GregorianCalendar();
		now.setTime(new Date());
		this.doneDate = now;
	}
	
	public boolean isOverdue() {
		if (this.done) {
			return false;
		}
		Calendar now = new GregorianCalendar();
		now.setTime(new Date());
		this.isOverdue = now.getTimeInMillis() > this.dueDate.getTimeInMillis(); 
		return this.isOverdue;
	}
	
	public boolean hasBeenOverdue() {
		return this.isOverdue;
	}
	
	public Duration getOverdueDuration() {
		if (!this.isOverdue) {
			return Duration.ZERO;
		} else if (this.done) {
			return Duration.ofMillis(doneDate.getTimeInMillis() - dueDate.getTimeInMillis());
		} else {
			Calendar now = new GregorianCalendar();
			now.setTime(new Date());
			return Duration.ofMillis(now.getTimeInMillis() - dueDate.getTimeInMillis());
		}
	}
}
