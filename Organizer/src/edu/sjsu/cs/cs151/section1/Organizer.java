

package edu.sjsu.cs.cs151.section1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A class for representing and storing various types of Fact objects.
 * 
 * @author Elton R. Sanchez for SJSU, CS151-Section 1 under Dr. Avila, (C) 2003 All Rights Reserved
 * @version 06/23/2018
 */
public class Organizer {
	// data structures for holding info; hidden away from user
	
	private static List<Fact> factList;
	private static FactType factMode;
	
	public Organizer() {
		this.factMode = FactType.FACTTYPE_GENERIC;
		this.factList = new ArrayList<Fact>();
	}
	
	public Organizer(List<Fact> newFacts) {
		this.factList = newFacts;
		this.factMode = FactType.FACTTYPE_GENERIC;
	}
	
	public void setFactMode(FactType newMode) {
		this.factMode = newMode;
	}
	
	public FactType getFactMode() {
		return this.factMode;
	}
	
	public void executeCommand(OrganizerCommand command, FactType factType, String filename) throws IOException {
		Fact tempFact = new Fact();
		
	}
}
